# Interactive Simplicial Filtrations
The notebook `interactive_simplicial_filtrations.ipynb`
lets you generate and plot filtered Delaunay, Cech and Rips complexes and their persistence diagram:

<img src="https://i.imgur.com/uxuEfgD.gif"></img>
